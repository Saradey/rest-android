package com.evgeny.goncharov.presentetionrestfirst.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Evgeny Goncharov on 23/11/2018.
 * jtgn@yandex.ru
 */

public class Def {

    @SerializedName("text")
    private String text;

    @SerializedName("pos")
    private String pos;

    @SerializedName("ts")
    private String ts;


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }
}
