package com.evgeny.goncharov.presentetionrestfirst;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Evgeny Goncharov on 23/11/2018.
 * jtgn@yandex.ru
 */

public class ActivityOkHttpClient extends AppCompatActivity {

    private final OkHttpClient client = new OkHttpClient();

    private TextView textView;

    public final String URL = "https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=%s&lang=%s&text=%s";

    public final String TEXT = "time";

    public final String KEY = "dict.1.1.20181024T164431Z.7ca2396a4d805bb5.396960d0fd7f1b235572c4a928d5101038f32c1c";

    public final String LANG = "en-ru";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_http_url_connection);

        textView = findViewById(R.id.response);

        //мы создаем новый бекграунд поток
        new Thread(new Runnable() {
            @Override
            public void run() {

                //создаем Request в котором содержится наш http url
                Request request = new Request.Builder()
                        .url(String.format(URL, KEY, LANG, TEXT))
                        .build();

                //функция newCall возвращает Call в который мы через функцию enqueue
                //создаем интерфейс Callback, когда к нам приходит ответ с сервера
                //в фунцию onResponse мы передаем класс MyResponse, то есть наш ответ
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NonNull Call call, @NonNull IOException e) {
                        System.out.println("Ошибка ответа");
                    }

                    @Override
                    public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                        String jsonResponse = response.body().string();

                        try {
                            final JSONObject jsonObject = new JSONObject(jsonResponse);

                            //UI поток
                            ActivityOkHttpClient.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    try {

                                        JSONArray jsonArray = (JSONArray) jsonObject.get("def");

                                        textView.setText(jsonArray.getString(0));

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            });


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });


            }
        }).start();


    }


}
