package com.evgeny.goncharov.presentetionrestfirst;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ActivityHttpURLConnection extends AppCompatActivity {


    public static final String
            BASE_URL = "https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=dict.1.1.20181024T164431Z.7ca2396a4d805bb5.396960d0fd7f1b235572c4a928d5101038f32c1c&lang=en-ru&text=time";

    private TextView textView;
    private StringBuilder rawData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_http_url_connection);


        textView = findViewById(R.id.response);

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    //URL url = new
                    // URL("https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=dict.1.1.20181024T164431Z.7ca2396a4d805bb5.396960d0fd7f1b235572c4a928d5101038f32c1c&lang=en-ru&text=time");

                    //тоже самое
                    //javoвый класс который позволяет получить клиента на соединения
                    URL url = new URL(BASE_URL);

                    //получаем клиента
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    //мы можем установить нашему клиенту таймаут подключения
                    connection.setConnectTimeout(1000);

                    //метол гет по умолчанию
                    //connection.setRequestMethod("GET");

                    //получаем поток данных прешедших из сети
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    //если класс String часто меняется, это будет негативно влиять на производительность
                    //StringBuilder удобный класс, для работы со строкой, он увеличивает скорость работы
                    //со строками
                    rawData = new StringBuilder();

                    //tempVariable класс для считывания строк из BufferedReader
                    String tempVariable = "";

                    //читаем по строчно
                    while ((tempVariable = reader.readLine()) != null) {
                        //ложим в наш StringBuilder результат
                        rawData.append(tempVariable).append("\n");
                    }
                    //обязательно закройте BufferedReader, иначе будет утечка памяти
                    reader.close();

                    //выводим наш результат запроса в Log.d
                    Log.d("MyRestTest", rawData.toString());

                    //все действия с UI интерфейсом осущесвтялются в UI потоке, если вы попытаетесь
                    //сделать монипуляцию на UI элементами в бекграунд потоке, произойдет падение
                    //приложения

                    //UI поток
                    ActivityHttpURLConnection.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            textView.setText(rawData.toString());
                        }
                    });


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();


    }


}






