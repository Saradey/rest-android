package com.evgeny.goncharov.presentetionrestfirst.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Evgeny Goncharov on 23/11/2018.
 * jtgn@yandex.ru
 */

public class MyResponse {

    @SerializedName("def")
    @Expose
    public List<Def> list = new ArrayList<>();

}
