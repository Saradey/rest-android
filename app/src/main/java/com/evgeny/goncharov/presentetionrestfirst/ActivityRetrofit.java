package com.evgeny.goncharov.presentetionrestfirst;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.widget.TextView;

import com.evgeny.goncharov.presentetionrestfirst.model.Def;
import com.evgeny.goncharov.presentetionrestfirst.model.MyResponse;

import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Evgeny Goncharov on 23/11/2018.
 * jtgn@yandex.ru
 */

public class ActivityRetrofit extends AppCompatActivity {

    private final String BASE_URL = "https://dictionary.yandex.net/api/v1/dicservice.json/";

    public final String TEXT = "time";

    public final String KEY = "dict.1.1.20181024T164431Z.7ca2396a4d805bb5.396960d0fd7f1b235572c4a928d5101038f32c1c";

    public final String LANG = "en-ru";

    private TextView textView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_http_url_connection);

        textView = findViewById(R.id.response);

        //создаем поток
        new Thread(new Runnable() {
            @Override
            public void run() {

                //создаем класс интерактор который загружается в OkHttpClient
                //основное предназначения интерактора, мониторить http запросы и http ответы
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                    @Override
                    public void log(String message) {
                        Log.d("RestLog", message);
                    }
                });

                //http состоит из "тела" ответа, мы же устанавливаем на уровне мониторинга тела ответа
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

                //Создаем сам OkHttpClient и загружаем в него интерактор
                OkHttpClient client = new OkHttpClient.Builder()
                        .addInterceptor(interceptor)
                        .build();

                //addConverterFactory позволяет нам серелизовать данные в классы благодаря этому
                //нам не придется парсить строку и руками создавать классы, они создаются за нас
                //baseUrl загружаем URL
                //в фунцию client мы загружаем наш OkHttpClient
                //функция build собирает наш Retrofit класс (паттерн строитель)
                //функция create создает класс Endpoints, который делает запрос и получает колбек
                Endpoints netApi = new Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(BASE_URL)
                        .client(client)
                        .build()
                        .create(Endpoints.class);


                //для того что бы сформировать запрос нам необходимо создать map
                //в который мы положим ключ, значение
                Map<String, String> map = new HashMap<>();
                map.put("key", KEY);
                map.put("lang", LANG);
                map.put("text", TEXT);

                //здесь происходит запрос и прилетает ответ
                netApi.getResponse(map).enqueue(new Callback<MyResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<MyResponse> call,
                                           @NonNull Response<MyResponse> response) {
                        if(response.isSuccessful()){
                            final Def def = response.body().list.get(1);

                            //UI поток
                            ActivityRetrofit.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    StringBuilder tramp= new StringBuilder(def.getText()
                                            + "\n"
                                            + def.getPos()
                                            + "\n"
                                            + def.getTs());

                                    textView.setText(tramp.toString());
                                }
                            });
                        }


                    }

                    @Override
                    public void onFailure(@NonNull Call<MyResponse> call, @NonNull Throwable t) {
                        Log.d("RestLog", t.getMessage());
                    }
                });


            }
        }).start();


    }


}
