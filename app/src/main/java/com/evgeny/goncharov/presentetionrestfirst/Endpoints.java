package com.evgeny.goncharov.presentetionrestfirst;

import com.evgeny.goncharov.presentetionrestfirst.model.Def;
import com.evgeny.goncharov.presentetionrestfirst.model.MyResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by Evgeny Goncharov on 23/11/2018.
 * jtgn@yandex.ru
 */

public interface Endpoints {

    @GET("lookup?")
    Call<MyResponse> getResponse(@QueryMap Map<String, String> request);

}
